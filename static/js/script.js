const rating = document.querySelectorAll('.rating');
rating.forEach(function(e){
    
    let starCounter = e.getAttribute('data-star');
    let stars = '';
    for( var i = 0; i < 5; i++ ){
        if( starCounter > i ){
            stars += `<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.59789 1.8541C9.19659 0.0114827 11.8034 0.0114794 12.4021 1.8541L13.3064 4.63729C13.5742 5.46133 14.3421 6.01925 15.2085 6.01925H18.135C20.0724 6.01925 20.878 8.49848 19.3105 9.63729L16.943 11.3574C16.242 11.8667 15.9487 12.7694 16.2165 13.5935L17.1208 16.3766C17.7195 18.2193 15.6105 19.7515 14.0431 18.6127L11.6756 16.8926C10.9746 16.3833 10.0254 16.3833 9.32443 16.8926L6.95691 18.6127C5.38948 19.7515 3.28052 18.2193 3.87923 16.3766L4.78354 13.5935C5.05129 12.7694 4.75797 11.8667 4.057 11.3574L1.68947 9.63729C0.122046 8.49848 0.9276 6.01925 2.86505 6.01925H5.79146C6.65792 6.01925 7.42583 5.46133 7.69357 4.63729L8.59789 1.8541Z" fill="url(#paint0_linear_5395_63836)"/>
                        <defs>
                        <linearGradient id="paint0_linear_5395_63836" x1="10.5" y1="-4.00068" x2="10.5" y2="25.0007" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFE61C"/>
                        <stop offset="1" stop-color="#FFA929"/>
                        </linearGradient>
                        </defs>
                    </svg>`;
        } else {
            stars += `<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.07341 2.00861C9.52244 0.626645 11.4776 0.626645 11.9266 2.00861L12.8309 4.79179C13.1656 5.82185 14.1255 6.51925 15.2085 6.51925H18.135C19.588 6.51925 20.1922 8.37868 19.0166 9.23278L16.6491 10.9529C15.7729 11.5895 15.4062 12.7179 15.7409 13.748L16.6452 16.5312C17.0943 17.9131 15.5126 19.0623 14.337 18.2082L11.9695 16.4881C11.0932 15.8515 9.90675 15.8515 9.03054 16.4881L6.66302 18.2082C5.48745 19.0623 3.90573 17.9131 4.35476 16.5312L5.25907 13.748C5.59375 12.7179 5.22711 11.5895 4.35089 10.9529L1.98337 9.23278C0.807798 8.37868 1.41196 6.51925 2.86505 6.51925H5.79146C6.87453 6.51925 7.83442 5.82185 8.1691 4.7918L9.07341 2.00861Z" stroke="url(#paint0_linear_5395_63840)"/>
                    <defs>
                    <linearGradient id="paint0_linear_5395_63840" x1="10.5" y1="-4.00068" x2="10.5" y2="25.0007" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#FFE61C"/>
                    <stop offset="1" stop-color="#FFA929"/>
                    </linearGradient>
                    </defs>
                    </svg>`;
        }
    }
    e.innerHTML = stars;
});

// ACCORDION
function collapseToggle(e){
    const item = e.target.closest('.accordion-item');
    item.classList.toggle('open');
}
const acc = document.querySelectorAll('.accordion-item');
acc.forEach(function(item){
    const arrow = item.querySelector('.accordion-collapse');
    arrow.addEventListener('click', collapseToggle);
});

