<?php

    /**
     * Home Page Product Carousel Constants
     */

    define('HPPC_LABEL_BEST_SELLERS', 'Best Sellers');
    define('HPPC_LABEL_NEW', 'New');
    define('HPPC_LABEL_FEATURED', 'Featured');
    define('HPPC_LABEL_SPECIALS', 'Specials');
    define('TITLE_HOME_TAB_SPECIALS', 'Specials');
    define('TITLE_HOME_TAB_BEST_SELLERS', 'Best Sellers');
    define('TITLE_HOME_TAB_FEATURED', 'Featured Items');
    define('TITLE_HOME_TAB_NEW', 'New Products');
    define('TEXT_BADGE_NEW', 'New');
    define('TEXT_BADGE_SALE', 'Sale');

?>
