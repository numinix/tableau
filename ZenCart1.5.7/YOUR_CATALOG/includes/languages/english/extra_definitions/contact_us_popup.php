<?php 
	define('HEADING_CONTACT_US_POPUP_TITLE', 'Send Us A Message');
	define('HEADING_CONTACT_US_POPUP_SUBTITLE', 'We\'d Like To Hear From You');
	define('HEADING_CONTACT_US_FULL_NAME', 'Full Name');
	define('HEADING_CONTACT_US_EMAIL', 'Email');
	define('HEADING_CONTACT_US_MESSAGE', 'Message');
	define('HEADING_CONTACT_US_SEND_MESSAGE', 'Send Message');
	define('HEADING_CONTACT_US_CANCEL', 'Cancel');
	define('CONTACT_US_POP_UP_LINK', 'Contact Us');
?>