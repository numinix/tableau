<?php

if(!function_exists('zen_admin_demo')){
/**
 * Check if restricted-use demo mode is active
 */
  function zen_admin_demo() {
    return (defined('ADMIN_DEMO') && ADMIN_DEMO == '1') ? TRUE : FALSE;
  }
}