SET @template_configuration_group_id=0;
SELECT @template_configuration_group_id:=template_configuration_group_id
FROM template_configuration_group
WHERE template_configuration_group_title = 'tableau'
LIMIT 1;
DELETE FROM template_configuration WHERE template_configuration_group_id = @template_configuration_group_id;
DELETE FROM template_configuration_group WHERE template_configuration_group_id = @template_configuration_group_id;

DELETE FROM admin_pages WHERE page_key = 'configTableau' LIMIT 1;