<?php

//hideCategories
$define = [
	'TEXT_EDIT_HIDE_STATUS' => 'Category visibility: ',
	'TEXT_EDIT_HIDE_NORMAL' => 'Normal',
	'TEXT_EDIT_HIDE_NOMENU' => 'Hidden from main menus',
	'TEXT_EDIT_HIDE_HIDDEN' => 'Hidden (direct link only)',
	'ICON_FOLDER_NORMAL' => 'Normal Folder',
	'ICON_FOLDER_HIDDEN' => 'Hidden Folder',
	'ICON_FOLDER_NOMENU' => 'Hidden from Menus'
];

return $define;
?>