<?php
$define = [
	'IH_RESIZE_TITLE' => 'IH resize images',
	'IH_RESIZE_TEXT' => 'Select either -no- which is old Zen-Cart behaviour or -yes- to activate automatic resizing and caching of images. --Note: If you select -no-, all of the Image Handler specific image settings will be unavailable including: image filetype selection, background colors, compression, image hover, and watermarking-- If you want to use ImageMagick you have to specify the location of the <strong>convert</strong> binary in <em>includes/extra_configures/bmz_image_handler_conf.php</em>.',

	'SMALL_IMAGE_FILETYPE_TITLE' => 'IH small images filetype',
	'SMALL_IMAGE_FILETYPE_TEXT' => 'Select one of -jpg-, -gif- or -png-. Older versions of Internet Explorer -v6.0 and older- will have issues displaying -png- images with transparent areas. You better stick to -gif- for transparency if you MUST support older versions of Internet Explorer. However -png- is a MUCH BETTER format for transparency. Use -jpg- or -png- for larger images. -no_change- is old zen-cart behavior, use the same file extension for small images as uploaded image',

	'SMALL_IMAGE_BACKGROUND_TITLE' => 'IH small images background',
	'SMALL_IMAGE_BACKGROUND_TEXT' => 'If converted from an uploaded image with transparent areas, these areas become the specified color. Set to -transparent- to keep transparency.',

	'SMALL_IMAGE_QUALITY_TITLE' => 'IH small images compression quality',
	'SMALL_IMAGE_QUALITY_TEXT' => 'Specify the desired image quality for small jpg images, decimal values ranging from 0 to 100. Higher is better quality and takes more space. Default is 85 which is ok unless you have very specific needs.',

	'WATERMARK_SMALL_IMAGES_TITLE' => 'IH small images watermark',
	'WATERMARK_SMALL_IMAGES_TEXT' => 'Set to -yes-, if you want to show watermarked small images instead of unmarked small images.',

	'ZOOM_SMALL_IMAGES_TITLE' => 'IH small images zoom on hover',
	'ZOOM_SMALL_IMAGES_TEXT' => 'Set to -yes-, if you want to enable a nice zoom overlay while hovering the mouse pointer over small images.',

	'ZOOM_IMAGE_SIZE_TITLE' => 'IH small images zoom on hover size',
	'ZOOM_IMAGE_SIZE_TEXT' => 'Set to -Medium-, if you want to the zoom on hover display to use the medium sized image. Otherwise, to use the large sized image on hover, set to -Large-',

	'MEDIUM_IMAGE_FILETYPE_TITLE' => 'IH medium images filetype',
	'MEDIUM_IMAGE_FILETYPE_TEXT' => 'Select one of -jpg-, -gif- or -png-. Older versions of Internet Explorer -v6.0 and older- will have issues displaying -png- images with transparent areas. You better stick to -gif- for transparency if you MUST support older versions of Internet Explorer. However -png- is a MUCH BETTER format for transparency. Use -jpg- or -png- for larger images. -no_change- is old zen-cart behavior, use the same file extension for medium images as uploaded image-s.',

	'MEDIUM_IMAGE_BACKGROUND_TITLE' => 'IH medium images background',
	'MEDIUM_IMAGE_BACKGROUND_TEXT' => 'If converted from an uploaded image with transparent areas, these areas become the specified color. Set to -transparent- to keep transparency.',

	'MEDIUM_IMAGE_QUALITY_TITLE' => 'IH medium images compression quality',
	'MEDIUM_IMAGE_QUALITY_TEXT' => 'Specify the desired image quality for medium jpg images, decimal values ranging from 0 to 100. Higher is better quality and takes more space. Default is 85 which is ok unless you have very specific needs.',

	'WATERMARK_MEDIUM_IMAGES_TITLE' => 'IH medium images watermark',
	'WATERMARK_MEDIUM_IMAGES_TEXT' => 'Set to -yes-, if you want to show watermarked medium images instead of unmarked medium images.',

	'LARGE_IMAGE_FILETYPE_TITLE' => 'IH large images filetype',
	'LARGE_IMAGE_FILETYPE_TEXT' => 'Select one of -jpg-, -gif- or -png-. Older versions of Internet Explorer -v6.0 and older- will have issues displaying -png- images with transparent areas. You better stick to -gif- for transparency if you MUST support older versions of Internet Explorer. However -png- is a MUCH BETTER format for transparency. Use -jpg- or -png- for larger images. -no_change- is old zen-cart behavior, use the same file extension for large images as uploaded image-s.',

	'LARGE_IMAGE_BACKGROUND_TITLE' => 'IH large images background',
	'LARGE_IMAGE_BACKGROUND_TEXT' => 'If converted from an uploaded image with transparent areas, these areas become the specified color. Set to -transparent- to keep transparency.',

	'LARGE_IMAGE_QUALITY_TITLE' => 'IH large images compression quality',
	'LARGE_IMAGE_QUALITY_TEXT' => 'Specify the desired image quality for large jpg images, decimal values ranging from 0 to 100. Higher is better quality and takes more space. Default is 85 which is ok unless you have very specific needs.',

	'WATERMARK_LARGE_IMAGES_TITLE' => 'IH large images watermark',
	'WATERMARK_LARGE_IMAGES_TEXT' => 'Set to -yes-, if you want to show watermarked large images instead of unmarked large images.',

	'LARGE_IMAGE_MAX_WIDTH_TITLE' => 'IH large images maximum width',
	'LARGE_IMAGE_MAX_WIDTH_TEXT' => 'Specify a maximum width for your large images. If width and height are empty or set to 0, no resizing of large images is done.',

	'LARGE_IMAGE_MAX_HEIGHT_TITLE' => 'IH large images maximum height',
	'LARGE_IMAGE_MAX_HEIGHT_TEXT' => 'Specify a maximum height for your large images. If width and height are empty or set to 0, no resizing of large images is done.',

	'WATERMARK_GRAVITY_TITLE' => 'IH watermark gravity',
	'WATERMARK_GRAVITY_TEXT' => 'Select the position for the watermark relative to the image-s canvas. Default is <strong>Center</Strong>.',

	'IH_VERSION_TITLE' => 'IH version',
	'IH_VERSION_TEXT' => 'IH Version is stored but not shown on configuration menus'
];

return $define;