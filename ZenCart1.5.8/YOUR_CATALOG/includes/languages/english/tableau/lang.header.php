<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: header.php 2940 2006-02-02 04:29:05Z drbyte $
 */

// header text in includes/header.php
$define = [
  'HEADER_TITLE_CREATE_ACCOUNT' => 'Create Account',
  'HEADER_TITLE_MY_ACCOUNT' => 'My Account',
  'HEADER_TITLE_CART_CONTENTS' => 'Shopping Cart:',
  'HEADER_TITLE_CHECKOUT' => 'Checkout',
  'HEADER_TITLE_CONTACT_US' => 'Contact Us',
  'HEADER_TITLE_TOP' => 'Top',
  'HEADER_TITLE_CATALOG' => 'Home',
  'HEADER_TITLE_LOGOFF' => 'Log Out',
  'HEADER_TITLE_LOGIN' => 'Login / Register',
  'HEADER_TITLE_WELCOME' => 'Welcome',
  'HEADER_TITLE_SHOPPING_CART' => 'Shopping Cart',
  'HEADER_TITLE_CATEGORIES' => 'Categories',

// added defines for header alt and text
  'HEADER_ALT_TEXT' => 'Tableau',
  'HEADER_SALES_TEXT' => 'TagLine Here',
  'HEADER_LOGO_WIDTH' => '192px',
  'HEADER_LOGO_HEIGHT' => '64px',
  'HEADER_LOGO_IMAGE' => 'logo.png',

// header Search Button/Box Search Button
  'HEADER_SEARCH_BUTTON' =>'Search',
  'HEADER_SEARCH_DEFAULT_TEXT' => 'Enter key words'
];

return $define;