<?php

/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: index.php 19537 2011-09-20 17:14:44Z drbyte $
 */
$define = [
  'TITLE_HOME_BLOG' => 'Our Blog',
  'TITLE_HOME_TWITTER_FEED' => 'Twitter Feed',
  'TEXT_HOME_INFO_UNCONNECTED' => 'Unable to connect to blog at this time.',

  /* Tabs */    
  'TITLE_HOME_TAB_SPECIALS' => 'Specials',
  'TITLE_HOME_TAB_BEST_SELLERS' => 'Best Sellers',
  'TITLE_HOME_TAB_FEATURED' => 'Featured Items',
  'TITLE_HOME_TAB_NEW' => 'New Products'
];
  
return $define;  