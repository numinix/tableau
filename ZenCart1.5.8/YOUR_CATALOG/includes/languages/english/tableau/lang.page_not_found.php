<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: page_not_found.php 3159 2006-03-11 01:35:04Z drbyte $
 */
$define = [
	'NAVBAR_TITLE' => '404 Error',
	'HEADING_TITLE' => 'Error: Page Not Found',
	'TEXT_INFORMATION' => '',
	'PAGE_ACCOUNT' => 'My Account',
	'PAGE_ACCOUNT_EDIT' => 'Account Information',
	'PAGE_ADDRESS_BOOK' => 'Address Book',
	'PAGE_ACCOUNT_HISTORY' => 'Order History',
	'PAGE_ACCOUNT_NOTIFICATIONS' => 'Newsletter Subscriptions',
	'PAGE_SHOPPING_CART' => 'Shopping Cart',
	'PAGE_CHECKOUT_SHIPPING' => 'Checkout',
	'PAGE_ADVANCED_SEARCH' => 'Advanced Search',
	'PAGE_PRODUCTS_NEW' => 'New Products',
	'PAGE_SPECIALS' => 'Specials',
	'PAGE_REVIEWS' => 'Reviews'
];

return $define;