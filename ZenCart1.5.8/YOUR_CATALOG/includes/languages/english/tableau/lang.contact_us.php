<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2011 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: contact_us.php 18695 2011-05-04 05:24:19Z drbyte $
 */
$define = [
	'HEADING_TITLE' => 'Contact Us',
	'NAVBAR_TITLE' => 'Contact Us',
	'TEXT_SUCCESS' => 'Your message has been successfully sent.',
	'EMAIL_SUBJECT' => 'Website Inquiry from ' . STORE_NAME,

	'ENTRY_NAME' => 'Name:',
	'ENTRY_EMAIL' => 'Email:',
	'ENTRY_SUBJECT' => 'Subject:',
	'ENTRY_ENQUIRY' => 'Message:',

	'SEND_TO_TEXT' =>'Send Email To:',
	'ENTRY_EMAIL_NAME_CHECK_ERROR' =>'Sorry, is your name correct? Our system requires a minimum of ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters. Please try again.',
	'ENTRY_EMAIL_SUBJECT_CHECK_ERROR' => 'Sorry, did you forget to enter a subject? Our system requires a subject. Please try again.',
	'ENTRY_EMAIL_CONTENT_CHECK_ERROR' =>'Did you forget your message? We would like to hear from you. You can type your comments in the text area below.',

	'NOT_LOGGED_IN_TEXT' => 'Not logged in'
];

return $define;