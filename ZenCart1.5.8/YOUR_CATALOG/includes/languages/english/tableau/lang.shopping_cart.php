<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: shopping_cart.php 3183 2006-03-14 07:58:59Z birdbrain $
 */
$define = [
	'NAVBAR_TITLE' => 'Shopping Cart',
	'HEADING_TITLE' => 'Shopping Cart',
	'HEADING_TITLE_EMPTY' => 'Your Shopping Cart',
	'TEXT_INFORMATION' => '',
	'TABLE_HEADING_REMOVE' => 'Remove',
	'TABLE_HEADING_QUANTITY' => 'Quantity',
	'TABLE_HEADING_MODEL' => 'Model',
	'TABLE_HEADING_PRICE' =>'Price',
	'TEXT_CART_EMPTY' => 'Your Shopping Cart is empty.',
	'SUB_TITLE_SUB_TOTAL' => 'Sub-Total:',
	'SUB_TITLE_TOTAL' => 'Total:',

	'OUT_OF_STOCK_CANT_CHECKOUT' => 'Products marked with ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' are out of stock or there are not enough in stock to fill your order. Please change the quantity of products marked with (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '). Thank you',
	'OUT_OF_STOCK_CAN_CHECKOUT' => 'Products marked with ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' are out of stock. Items not in stock will be placed on backorder.',

	'TEXT_TOTAL_ITEMS' => 'Total Items: ',
	'TEXT_TOTAL_WEIGHT' => '&nbsp;&nbsp;Weight: ',
	'TEXT_TOTAL_AMOUNT' => '&nbsp;&nbsp;Amount: ',

	'TEXT_VISITORS_CART' => '<a href="javascript:session_win();">[help (?)]</a>',
	'TEXT_OPTION_DIVIDER' => '&nbsp;-&nbsp;'
];

return $define;