<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: product_music_info.php 4336 2006-09-01 20:04:54Z drbyte $
 *
 * Modified by Pavel Palek (2P) aka Dedek (zencart@palek.net) - 2007-03-19 - Average Product Rating
 */
$define = [
	'TEXT_PRODUCT_NOT_FOUND' => 'Sorry, the product was not found.',
	'TEXT_CURRENT_REVIEWS' => 'Current Reviews:',
	'TEXT_RECORD_COMPANY_URL' => 'For more information, please visit the Record Company\'s <a href="%s" target="_blank">webpage</a>.',
	'TEXT_ARTIST_URL' => 'For more information, please visit the Artist\'s <a href="%s" target="_blank">webpage</a>.',
	'TEXT_DATE_ADDED' => 'This product was added to our catalog on %s.',
	'TEXT_DATE_AVAILABLE' => 'This product will be in stock on %s.',
	'TEXT_ALSO_PURCHASED_PRODUCTS' => 'Customers who bought this product also purchased...',
	'TEXT_PRODUCT_OPTIONS' => 'Please Choose: ',
	'TEXT_PRODUCT_RECORD_COMPANY' => 'Record Company: ',
	'TEXT_PRODUCT_ARTIST' => 'Artist: ',
	'TEXT_PRODUCT_MUSIC_GENRE' => 'Music Genre: ',
	'TEXT_PRODUCT_WEIGHT' => 'Shipping Weight: ',
	'TEXT_PRODUCT_QUANTITY' => ' Units in Stock',
	'TEXT_PRODUCT_MODEL' => 'Model: ',
	'TEXT_PRODUCT_COLLECTIONS' => 'Media Collection: ',



	// previous next product
	'PREV_NEXT_PRODUCT' => 'Product ',
	'PREV_NEXT_FROM' => ' from ',
	'IMAGE_BUTTON_PREVIOUS' =>'Previous Item',
	'IMAGE_BUTTON_NEXT' =>'Next Item',
	'IMAGE_BUTTON_RETURN_TO_PRODUCT_LIST' =>'Back to Product List',

	// missing products
	//'TABLE_HEADING_NEW_PRODUCTS' => 'New Products For %s',
	//'TABLE_HEADING_UPCOMING_PRODUCTS' => 'Upcoming Products',
	//'TABLE_HEADING_DATE_EXPECTED' => 'Date Expected',

	'TEXT_ATTRIBUTES_PRICE_WAS' =>' [was: ',
	'TEXT_ATTRIBUTE_IS_FREE' =>' now is: Free]',
	'TEXT_ONETIME_CHARGE_SYMBOL' => ' *',
	'TEXT_ONETIME_CHARGE_DESCRIPTION' => ' One time charges may apply',
	'TEXT_ATTRIBUTES_QTY_PRICE_HELP_LINK' =>'Quantity Discounts Available',
	'ATTRIBUTES_QTY_PRICE_SYMBOL' => zen_image(DIR_WS_TEMPLATE_ICONS . 'icon_status_green.gif', TEXT_ATTRIBUTES_QTY_PRICE_HELP_LINK, 10, 10) . '&nbsp;',

	'TEXT_CURRENT_REVIEWS_RATING' => 'Average Rating:' // 2P added - Average Product Rating
];

return $define;