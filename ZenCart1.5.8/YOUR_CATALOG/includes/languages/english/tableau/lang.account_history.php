<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account_history.php 2989 2006-02-08 04:07:25Z drbyte $
 */
$define = [
	'NAVBAR_TITLE_1' => 'My Account',
	'NAVBAR_TITLE_2' => 'Order History',

	'HEADING_TITLE' => 'Order History',

	'TEXT_ORDER_NUMBER' => 'Order Number',
	'TEXT_ORDER_STATUS' => 'Order Status',
	'TEXT_ORDER_DATE' => 'Order Date',
	'TEXT_ORDER_SHIPPED_TO' => 'Shipped To',
	'TEXT_ORDER_BILLED_TO' => 'Billed To',
	'TEXT_ORDER_PRODUCTS' => 'Products',
	'TEXT_ORDER_COST' => 'Order Cost',
	'TEXT_VIEW_ORDER' => 'View Order',

	'TEXT_NO_PURCHASES' => 'You have not yet made any purchases.'
];

return $define;