<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account_history_info.php 3027 2006-02-13 17:15:51Z drbyte $
 */
$define = [
	'NAVBAR_TITLE' => 'My Account',
	'NAVBAR_TITLE_1' => 'My Account',
	'NAVBAR_TITLE_2' => 'History',
	'NAVBAR_TITLE_3' => 'Order #%s',

	'HEADING_TITLE' => 'Order Details',

	'HEADING_ORDER_NUMBER' => '<strong>Order Number:</strong> %s',
	'HEADING_ORDER_DATE' => 'Order Date:',
	'HEADING_ORDER_TOTAL' => 'Order Total:',

	'HEADING_DELIVERY_ADDRESS' => 'Shipped To:',
	'HEADING_SHIPPING_METHOD' => 'Shipping Method:',

	'HEADING_PRODUCTS' => 'Item',
	'HEADING_TAX' => 'Tax',
	'HEADING_TOTAL' => 'Price',
	'HEADING_IMAGE' => 'Image',

	'HEADING_BILLING_ADDRESS' => 'Billing To:',
	'HEADING_PAYMENT_METHOD' => 'Payment Method:',

	'HEADING_ORDER_HISTORY' => 'Order Status History',
	'TEXT_NO_COMMENTS_AVAILABLE' => 'No comments available.',
	'TABLE_HEADING_STATUS_DATE' => 'Date',
	'TABLE_HEADING_STATUS_ORDER_STATUS' => 'Status',
	'TABLE_HEADING_STATUS_COMMENTS' => 'Comments',
	'QUANTITY_SUFFIX' => '&nbsp;x  ',
	'ORDER_HEADING_DIVIDER' => '&nbsp;-&nbsp;',
	'TEXT_OPTION_DIVIDER' => '&nbsp;-&nbsp;'
];

return $define;