<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account_newsletters.php 3159 2006-03-11 01:35:04Z drbyte $
 */
$define = [
	'NAVBAR_TITLE_1' => 'My Account',
	'NAVBAR_TITLE_2' => 'Communications',

	'HEADING_TITLE' => 'Communications',

	'MY_NEWSLETTERS_TITLE' => 'Newsletter Subscriptions',
	'MY_EMAIL_PREFERENCES_TITLE' => 'E-mail Preferences',
	'MY_NEWSLETTERS_OPTION_1' => 'Subscribe me to the ' . STORE_NAME . ' Newsletter',
	'MY_NEWSLETTERS_OPTION_2' => 'Do not subscribe me to the ' . STORE_NAME . ' Newsletter',
	'MY_NEWSLETTERS_GENERAL_NEWSLETTER' => 'General Newsletter',
	'MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION' => 'Including store news, new products, special offers, and other promotional announcements.',

	'SUCCESS_NEWSLETTER_UPDATED' => 'Your newsletter subscriptions have been updated.'
];

return $define;