<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: address_book_process.php 9647 2008-09-17 00:10:08Z drbyte $
 */
$define = [

	'NAVBAR_TITLE_1' => 'My Account',
	'NAVBAR_TITLE_2' => 'Address Book',

	'NAVBAR_TITLE_ADD_ENTRY' => 'New Entry',
	'NAVBAR_TITLE_MODIFY_ENTRY' => 'Update Entry',
	'NAVBAR_TITLE_DELETE_ENTRY' => 'Delete Entry',

	'HEADING_TITLE_ADD_ENTRY' => 'Add New Entry',
	'HEADING_TITLE_MODIFY_ENTRY' => 'Update Entry',
	'HEADING_TITLE_DELETE_ENTRY' => 'Delete Entry',
	'HEADING_TITLE' => 'Address Details',

	'DELETE_ADDRESS_TITLE' => 'Delete Address',
	'DELETE_ADDRESS_DESCRIPTION' => 'Are you sure you would like to delete the selected address from your address book?',

	'NEW_ADDRESS_TITLE' => 'New Address Book Entry',

	'SELECTED_ADDRESS' => 'Selected Address',
	'SET_AS_PRIMARY' => 'Set as primary address.',

	'SUCCESS_ADDRESS_BOOK_ENTRY_DELETED' => 'The selected address has been successfully removed from your address book.',
	'SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED' => 'Your address book has been successfully updated.',

	'WARNING_PRIMARY_ADDRESS_DELETION' => 'The primary address cannot be deleted. Please set another address as the primary address and try again.',

	'ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY' => 'The address book entry does not exist.',
	'ERROR_ADDRESS_BOOK_FULL' => 'Your address book is full. Please delete an unneeded address to save a new one.'
];

return $define;