<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account.php 3595 2006-05-07 06:39:23Z drbyte $
 */

$define = [

'NAVBAR_TITLE' => 'My Account',
'HEADING_TITLE' => 'My Account Information',

'OVERVIEW_TITLE' => 'Overview',
'OVERVIEW_SHOW_ALL_ORDERS' => '(show all orders)',
'OVERVIEW_PREVIOUS_ORDERS' => 'Previous Orders',
'TABLE_HEADING_DATE' => 'Date',
'TABLE_HEADING_ORDER_NUMBER' => 'No.',
'TABLE_HEADING_SHIPPED_TO' => 'Ship To',
'TABLE_HEADING_STATUS' => 'Status',
'TABLE_HEADING_TOTAL' => 'Total',
'TABLE_HEADING_VIEW' => 'View',

'TEXT_ORDER_NUMBER' => 'Order Number',
'TEXT_ORDER_STATUS' => 'Order Status',
'TEXT_ORDER_DATE' => 'Order Date',
'TEXT_ORDER_SHIPPED_TO' => 'Shipped To',
'TEXT_ORDER_BILLED_TO' => 'Billed To',
'TEXT_ORDER_PRODUCTS' => 'Products',
'TEXT_ORDER_COST' => 'Order Cost',
'TEXT_VIEW_ORDER' => 'View Order',

'MY_ACCOUNT_TITLE' => 'My Account',
'MY_ACCOUNT_INFORMATION' => 'View or change my account information.',
'MY_ACCOUNT_ADDRESS_BOOK' => 'View or change entries in my address book.',
'MY_ACCOUNT_PASSWORD' => 'Change my account password.',

'MY_ORDERS_TITLE' => 'My Orders',
'MY_ORDERS_VIEW' => 'View the orders I have made.',

'EMAIL_NOTIFICATIONS_TITLE' => 'Email Notifications',
'EMAIL_NOTIFICATIONS_NEWSLETTERS' => 'Subscribe or unsubscribe from newsletters.',
'EMAIL_NOTIFICATIONS_PRODUCTS' => 'View or change my product notification list.'
];

return $define;