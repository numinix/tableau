<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: icon_names.php 4224 2006-08-24 01:41:50Z drbyte $
 */

// define the icon images used in the project
$define = [
	'ICON_IMAGE_ERROR' => 'error.gif',
	'ICON_IMAGE_WARNING' => 'warning.gif',
	'ICON_IMAGE_SUCCESS' => 'success.gif',
	'ICON_IMAGE_TRASH' => 'small_delete.gif',
	'ICON_IMAGE_UPDATE' => 'button_update_cart.gif',
	'ICON_IMAGE_TINYCART' => 'cart.gif',

	//alt tags for buttons
	'ICON_ERROR_ALT' => 'Error',
	'ICON_SUCCESS_ALT' => 'Success',
	'ICON_WARNING_ALT' => 'Warning',
	'ICON_TRASH_ALT' => 'Delete',
	'ICON_UPDATE_ALT' => 'Change your quantity by highlighting the number in the box, correcting the quantity and clicking this button.',
	'ICON_TINYCART_ALT' => 'Add this product to your cart by clicking here.',
];

return $define;