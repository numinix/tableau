<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: product_reviews_info.php 3027 2006-02-13 17:15:51Z drbyte $
 */
$define = [
	'NAVBAR_TITLE' => 'Reviews',
	//'SUB_TITLE_PRODUCT' => 'Product:',
	//'SUB_TITLE_FROM' => 'From:',
	//'SUB_TITLE_DATE' => 'Date:',
	//'SUB_TITLE_REVIEW' => 'Review:',
	//'SUB_TITLE_RATING' => 'Rating:',
	'TEXT_OF_5_STARS' => '',
	'TEXT_PRODUCT_INFO' => 'Take Me to the Details',
	'TEXT_REVIEW_ADDITIONAL' => 'Read More Reviews'
];

return $define;