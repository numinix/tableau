<?php 
$define = [
	'HEADING_CONTACT_US_POPUP_TITLE' => 'Send Us A Message',
	'HEADING_CONTACT_US_POPUP_SUBTITLE' => 'We\'d Like To Hear From You',
	'HEADING_CONTACT_US_FULL_NAME' => 'Full Name',
	'HEADING_CONTACT_US_EMAIL' => 'Email',
	'HEADING_CONTACT_US_MESSAGE' => 'Message',
	'HEADING_CONTACT_US_SEND_MESSAGE' => 'Send Message',
	'HEADING_CONTACT_US_CANCEL' => 'Cancel',
	'CONTACT_US_POP_UP_LINK' => 'Contact Us'
];

return $define;
?>