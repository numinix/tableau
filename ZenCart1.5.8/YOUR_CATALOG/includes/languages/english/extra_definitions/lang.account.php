<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: account.php 3595 2006-05-07 06:39:23Z drbyte $
 */
$define = [
	'OVERVIEW_TITLE' => 'Overview',
	'OVERVIEW_SHOW_ALL_ORDERS' => '(show all orders)',
	'OVERVIEW_PREVIOUS_ORDERS' => 'Previous Orders',
	'TABLE_HEADING_DATE' => 'Date',
	'TABLE_HEADING_ORDER_NUMBER' => 'No.',
	'TABLE_HEADING_SHIPPED_TO' => 'Ship To',
	'TABLE_HEADING_STATUS' => 'Status',
	'TABLE_HEADING_TOTAL' => 'Total',
	'TABLE_HEADING_VIEW' => 'View',

	'MY_ACCOUNT_ORDER_HISTORY' => 'Order History',
	'MY_ACCOUNT_INFORMATION' => 'Contact Info',
	'MY_ACCOUNT_ADDRESS_BOOK' => 'Address Book',
	'MY_ACCOUNT_PASSWORD' => 'Change Password',

	'MY_ORDERS_TITLE' => 'My Orders',
	'MY_ORDERS_VIEW' => 'View the orders I have made.',

	'EMAIL_NOTIFICATIONS_TITLE' => 'Email Notifications',
	'EMAIL_NOTIFICATIONS_NEWSLETTERS' => 'Communications',
	'EMAIL_NOTIFICATIONS_PRODUCTS' => 'View or change my product notification list.'
];

return $define;