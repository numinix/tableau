<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: header.php 2940 2006-02-02 04:29:05Z drbyte $
 */

// header text in includes/footer.php
$define = [
   'FOOTER_TITLE_CREATE_ACCOUNT' => 'Create Account',
   'FOOTER_TITLE_MY_ACCOUNT' => 'My Account',
   'FOOTER_TITLE_CART_CONTENTS' => 'Shopping Cart',
   'FOOTER_TITLE_CHECKOUT' => 'Checkout',
   'FOOTER_TITLE_CONTACT_US' => 'Contact Us',
   'FOOTER_TITLE_TOP' => 'Top',
   'FOOTER_TITLE_CATALOG' => 'Home',
   'FOOTER_TITLE_LOGOFF' => 'Log Out',
   'FOOTER_TITLE_LOGIN' => 'Login / Register',

   // Col 1
   'FOOTER_SHOP' => 'Shop',
   'FOOTER_CATEGORIES' => 'All Categories',

   // Col 2
   'FOOTER_CUSTOMER_SERVICES' => 'Customer Services',
   'FOOTER_DISCOUNT_COUPONS' => 'Discount Coupons',
   'FOOTER_GIFT_CERT' => 'Gift Certificate FAQ',

   // Col 3
   'FOOTER_POLICIES' => 'Polices &amp; Privacy',
   'FOOTER_SHIPPING' => 'Shipping &amp; Returns',
   'FOOTER_PRIVACY' => 'Privacy Policy',
   'FOOTER_CONDITIONS' => 'Conditions of Use',
   

   // Newsletter
   'FOOTER_NEWSLETTER' => 'Newsletter',
   'FOOTER_NEWSLETTER_PLACEHOLDER' => 'Enter email address',
   'FOOTER_SUBSCRIBE' => 'Subscribe',
   'FOOTER_UNSUBSCRIBE' => 'Newsletter Unsubscribe'
];

return $define;