<?php

    /**
     * Home Page Product Carousel Constants
     */
$define = [
    'HPPC_LABEL_BEST_SELLERS' => 'Best Sellers',
    'HPPC_LABEL_NEW' => 'New',
    'HPPC_LABEL_FEATURED' => 'Featured',
    'HPPC_LABEL_SPECIALS' => 'Specials',
    'TITLE_HOME_TAB_SPECIALS' => 'Specials',
    'TITLE_HOME_TAB_BEST_SELLERS' => 'Best Sellers',
    'TITLE_HOME_TAB_FEATURED' => 'Featured Items',
    'TITLE_HOME_TAB_NEW' => 'New Products',
    'TEXT_BADGE_NEW' => 'New',
    'TEXT_BADGE_SALE' => 'Sale'
];

return $define;

?>
