<?php
/**
 * @package languageDefines
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: product_reviews_write.php 3159 2006-03-11 01:35:04Z drbyte $
 */
$define = [
'NAVBAR_TITLE' => 'Reviews',
'SUB_TITLE_FROM' => 'Written by:',
'SUB_TITLE_REVIEW' => 'Review',
'SUB_TITLE_RATING' => 'Overall Rating',

'TEXT_NO_HTML' => '<strong>NOTE:</strong>  HTML tags are not allowed.',
'TEXT_BAD' => 'Worst',
'TEXT_GOOD' => 'Best',
'TEXT_PRODUCT_INFO' => '',

'TEXT_APPROVAL_REQUIRED' => '<strong>NOTE:</strong>  Reviews require prior approval before they will be displayed',

'EMAIL_REVIEW_PENDING_SUBJECT' =>'Product Review Pending Approval: %s',
'EMAIL_PRODUCT_REVIEW_CONTENT_INTRO' =>'A Product Review for %s has been submitted and requires your approval.'."\n\n",
'EMAIL_PRODUCT_REVIEW_CONTENT_DETAILS' =>'Review Details: %s'
];

return $define;

?>